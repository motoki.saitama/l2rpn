# Overview

This solution deals with 2 situations.

- When there are no overloads, try to re-connect lines
- Where there are overloads, try to solve it

This submission is heavily based on the public submisison from TonyS (https://github.com/Tony-LS/l2rpn-public-submission).




