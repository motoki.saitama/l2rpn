
from .sim_forward_network import SimNetwork

from grid2op.Agent import BaseAgent
import numpy as np

TRACK1_NUMBER_OF_LINES = 59
INPUT_SIZE = TRACK1_NUMBER_OF_LINES + 1 + TRACK1_NUMBER_OF_LINES
OUTPUT_SIZE = TRACK1_NUMBER_OF_LINES

class SimAgent(BaseAgent):
    def __init__(self, action_space, net: SimNetwork, debug=False):
        """Initialize a new agent."""
        BaseAgent.__init__(self, action_space=action_space)
        self.net = net
        self.debug = debug

    def act(self, obs, reward, done=False):
        if self.debug:
            # https://grid2op.readthedocs.io/en/latest/observation.html

            self.debug = False
            print(len(obs.rho)) # 59
            # print(obs.rho)
            # print(len(obs.line_status))
            # print(obs.line_status)

        line_status = obs.line_status.astype(int)
        if line_status.sum() >= line_status.shape[0]: # all lines are connected
            return self.action_space({})  # do nothing

        rho = obs.rho
        rho_max = np.array([obs.rho.max()])
        rho_max_next = np.zeros(line_status.shape)

        time_before_cooldown_line = obs.time_before_cooldown_line
        change_line_status = np.zeros(line_status.shape).astype(int)
        for i, tc in enumerate(time_before_cooldown_line):
            if line_status[i] == 0:  # disconnected line
                rho_max_next[i] = 1.0
                if tc <= 0:
                    change_line_status[i] = 1
                    obs_next, _, done, _ = obs.simulate(
                        self.action_space({'set_line_status': change_line_status})
                    )
                    change_line_status[i] = 0

                    if not done:
                        rho_max_next[i] = obs_next.rho.max()

        net_input = np.concatenate((rho, rho_max, rho_max_next))
        net_output = self.net.predict(net_input)  # TODO: more exploration
        output_mask = np.where(line_status == 1)
        net_output[output_mask] = -1
        line_to_connect = np.argmax(net_output)

        change_line_status = np.zeros(line_status.shape).astype(int)
        change_line_status[line_to_connect] = 1
        return self.action_space({'set_line_status': change_line_status})
