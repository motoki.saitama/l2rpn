#!/usr/bin/env python3

from .sim_agent import SimAgent
from .sim_agent import INPUT_SIZE, OUTPUT_SIZE
from .sim_forward_network import SimNetwork

import os
import numpy as np
import pickle
import datetime

import argparse

from grid2op.MakeEnv import make
from grid2op.Runner import Runner
from grid2op.Reward import *
from grid2op.Action import *
from grid2op.Agent import BaseAgent

from evostra import EvolutionStrategy

DEBUG = True
if DEBUG:
    POPULATION_SIZE = 2

    DEFAULT_NB_ITERATIONS = 2
    DEFAULT_NB_THREADS = 1
    DEFAULT_NB_EPISODES_PER_AGENT = 2

    DEFAULT_MAX_STEPS = 2

else:
    POPULATION_SIZE = 20

    DEFAULT_NB_ITERATIONS = 10
    DEFAULT_NB_THREADS = 4
    DEFAULT_NB_EPISODES_PER_AGENT = 5

    DEFAULT_MAX_STEPS = -1



SIGMA = 0.1
LEARNING_RATE = 0.01
INITIAL_EXPLORATION = 1.0
FINAL_EXPLORATION = 0.0
EXPLORATION_DEC_STEPS = 1000000


VERBOSE = True
import datetime

class SimModel(object):
    def __init__(self, env,
                 iterations=DEFAULT_NB_ITERATIONS,
                 max_steps=DEFAULT_MAX_STEPS
                 ):
        self.env = env
        self.iterations = iterations
        self.max_steps = max_steps

        input_size = INPUT_SIZE
        output_size = OUTPUT_SIZE

        self.net = SimNetwork(layer_sizes=[input_size, 2*input_size, 2*output_size, output_size])

        self.es = EvolutionStrategy(self.net.get_weights(), self.get_reward,
                                    population_size=POPULATION_SIZE,
                                    sigma=SIGMA, learning_rate=LEARNING_RATE,
                                    decay=0.999,
                                    num_threads=DEFAULT_NB_THREADS)
        self.exploration = INITIAL_EXPLORATION

    def train(self, filename='weights.pkl'):
        print("-" * 80)
        print("Training ... ")
        self.es.run(self.iterations, print_step=1)

        print("Saving ...", filename)
        self.save(filename=filename)
        print("DONE!")
        print("-"*80)

    def save(self, filename='weights.pkl'):
        with open(filename, 'wb') as fp:
            pickle.dump(self.es.get_weights(), fp)

    def get_reward(self, weights):
        agent_net = SimNetwork(layer_sizes=self.net.get_layer_sizes())
        agent_net.set_weights(weights=weights)
        agent = SimAgent(self.env.action_space, agent_net, debug=VERBOSE)

        runner_params = self.env.get_params_for_runner()
        runner_params["verbose"] = False
        runner_params['agentClass'] = None

        runner = Runner(**runner_params, agentInstance=agent)
        res = runner.run(nb_episode=DEFAULT_NB_EPISODES_PER_AGENT,
                         max_iter=self.max_steps,
                         path_save=None,
                         nb_process=1,
                         pbar=False)

        total_reward = 0
        adjusted_reward = 0
        total_nb_steps = 0

        max_ts = 0
        len_res = len(res)

        for _, chron_name, cum_reward, nb_time_step, max_ts in res:
            total_reward += cum_reward / len_res
            adjusted_reward += (cum_reward - (1000 - nb_time_step) * 300) / len_res
            total_nb_steps += nb_time_step / len_res


        print(datetime.datetime.now())
        print("Outcome: {} / {} steps, {} original rewards, {} adjusted reward ".format(
            int(total_nb_steps), max_ts, int(total_reward), int(adjusted_reward)))

        return adjusted_reward


def main_func(args):
    # Create dataset env
    env = make(args.data_dir,
               reward_class=RedispReward,
               action_class=TopologyChangeAction)
    # Call evaluation interface
    model = SimModel(env,
                     iterations=args.iterations,
                     max_steps=args.max_steps)

    model.train()
    model.save(filename='weights_v0.pkl')


def cli():
    parser = argparse.ArgumentParser(description="Eval baseline HelloAgent")
    parser.add_argument("--data_dir", required=True,
                        help="Path to the dataset root directory")
    parser.add_argument("--iterations", required=False,
                        default=DEFAULT_NB_ITERATIONS, type=int,
                        help="Number of iterations to evaluate")
    parser.add_argument("--max_steps", required=False,
                        default=DEFAULT_MAX_STEPS, type=int,
                        help="Maximum number of steps per scenario")
    return parser.parse_args()


if __name__ == "__main__":
    args = cli()
    main_func(args)

