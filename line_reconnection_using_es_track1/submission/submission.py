from .sim_agent import SimAgent, INPUT_SIZE, OUTPUT_SIZE
from .sim_forward_network import SimNetwork

def make_agent(env, submission_dir):
    input_size = INPUT_SIZE
    output_size = OUTPUT_SIZE
    agent_net = SimNetwork(layer_sizes=[input_size, 2 * input_size, 2 * output_size, output_size])
    # agent_net.set_weights(weights=weights)
    agent_net.load(filename="{}/weights_v0.pkl".format(submission_dir))
    agent = SimAgent(env.action_space, agent_net, debug=True)
    return agent

