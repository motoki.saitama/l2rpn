Simple neuron network for reconnecting lines.

The idea is to train an agent using Evolution Strategies and deep learning models to learn how to walk (https://github.com/alirezamika/bipedal-es). Training code is available at sim_train.py.

