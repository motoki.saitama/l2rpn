from .ExpertAgent import ExpertAgent

def make_agent(env, submission_dir):
    agent = ExpertAgent(env.action_space,env.observation_space,"test","IEEE118_R2")
    return agent

