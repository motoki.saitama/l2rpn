# Overview

This solution runs expert agent with a time-computation-constraint.

This submission is heavily based on the public "Expert" submisison from Antoine Marot (team amarot).
More information at https://competitions.codalab.org/competitions/public_submissions/25426
